import 'package:dockerregistryexplorer/components/TagCard.dart';
import 'package:dockerregistryexplorer/data/repositories.dart';
import 'package:dockerregistryexplorer/data/repositorymodel.dart';
import 'package:flutter/material.dart';

class RepositoryDetails extends StatefulWidget {
  final RepositoriesDataSource repositories = RepositoriesDataSource();
  final RepositoryModel repo;

  RepositoryDetails({super.key, required this.repo});

  @override
  State<StatefulWidget> createState() => _RepositoryDetailsState();
}

class _RepositoryDetailsState extends State<RepositoryDetails> {
  @override
  void didUpdateWidget(covariant RepositoryDetails oldWidget) {
    super.didUpdateWidget(oldWidget);
    Future.wait([fetchRepositoryDetails()]);
  }

  Future fetchRepositoryDetails() async {
    var repoTags = await widget.repositories.fetchRepoTags(widget.repo.name);
    Map<String, String> tagsWithSha = {};
    for (var i = 0; i < repoTags.length; i++) {
      final tag = repoTags[i];
      final manifest =
          await widget.repositories.fetchVersionManifest(widget.repo.name, tag);
      tagsWithSha[tag] = manifest["digest"];
    }
    widget.repo.tags = tagsWithSha;
    setState(() {});
  }

  Future deleteByDigest(String sha256) async {
    await widget.repositories.deleteImage(widget.repo.name, sha256);
    await fetchRepositoryDetails();
  }

  @override
  Widget build(BuildContext context) {
    return Flexible(
        flex: 1,
        fit: FlexFit.loose,
        child: GridView.count(
          padding: const EdgeInsets.fromLTRB(0, 16, 0, 16),
          mainAxisSpacing: 16,
          crossAxisSpacing: 16,
          shrinkWrap: false,
          crossAxisCount: 3,
          children: widget.repo.tags.keys.map((key) {
            return TagCard(
              digest: widget.repo.tags[key].toString(),
              tagName: key,
              onDelete: () => Future.wait(
                  [deleteByDigest(widget.repo.tags[key].toString())]),
            );
          }).toList(),
        ));
  }
}
