import 'package:dockerregistryexplorer/data/repositories.dart';
import 'package:dockerregistryexplorer/data/repositorymodel.dart';
import 'package:dockerregistryexplorer/features/repository_details/repository_details.dart';
import 'package:flutter/material.dart';

class RepositoryList extends StatefulWidget {
  RepositoryList({super.key});

  final RepositoriesDataSource repositories = RepositoriesDataSource();

  @override
  State<StatefulWidget> createState() => _RepositoryListState();
}

class _RepositoryListState extends State<RepositoryList> {
  List<RepositoryModel> dockerRepos = [];
  RepositoryModel? repositoryModel;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => fetch());
  }

  void fetch() {
    Future.delayed(const Duration(seconds: 1), () async {
      final value = await widget.repositories.fetchRepositories();
      setState(() {
        dockerRepos = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () => fetch(),
        tooltip: 'Refresh Repository List',
        child: const Icon(Icons.refresh),
      ),
      body: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(16, 0, 0, 8),
            child: SizedBox(
              width: 300,
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: dockerRepos
                      .map((e) => Column(
                            children: [
                              MaterialButton(
                                onPressed: () {
                                  setState(() {
                                    repositoryModel = e;
                                  });
                                },
                                child: SizedBox(
                                  height: 65,
                                  child: Row(
                                    children: [
                                      Text(
                                        e.name,
                                        style: TextStyle(
                                            color:
                                                repositoryModel?.name == e.name
                                                    ? const Color(0xfAff7270)
                                                    : Colors.black,
                                            fontWeight:
                                                repositoryModel?.name == e.name
                                                    ? FontWeight.bold
                                                    : FontWeight.normal),
                                      ),
                                      const Spacer(),
                                      const Icon(Icons.arrow_right)
                                    ],
                                  ),
                                ),
                              ),
                              const Divider()
                            ],
                          ))
                      .toList(),
                ),
              ),
            ),
          ),
          const VerticalDivider(
            color: Colors.black12,
          ),
          repositoryModel != null
              ? RepositoryDetails(repo: repositoryModel!)
              : Container()
        ],
      ),
    );
  }
}
