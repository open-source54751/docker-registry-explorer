class RepositoryModel {
  final String name;
  Map<String, String> tags = {};
  String sha256Digest = "";

  RepositoryModel(this.name);
}
