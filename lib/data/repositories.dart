import 'package:dockerregistryexplorer/data/repositorymodel.dart';
import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class RepositoriesDataSource {
  static const baseAddress = "192.168.1.102:5050";

  Future<List<RepositoryModel>> fetchRepositories() async {
    final dockerRegistryUrl = Uri.http(baseAddress, "v2/_catalog");
    final data = await http.get(dockerRegistryUrl);
    final dataMap = jsonDecode(data.body);
    final repoList = dataMap["repositories"] as List<dynamic>;
    return repoList.map((e) => RepositoryModel(e.toString())).toList();
  }

  Future<List<String>> fetchRepoTags(String repoName) async {
    final dockerRegistryUrl = Uri.http(baseAddress, "v2/$repoName/tags/list");
    final data = await http.get(dockerRegistryUrl);
    final dataMap = jsonDecode(data.body);

    final dynamicList = dataMap["tags"] as List<dynamic>;

    return dynamicList.map((e) => e.toString()).toList();
  }

  Future<Map<String, dynamic>> fetchVersionManifest(
      String repoName, String tag) async {
    final dockerRegistryUrl =
        Uri.http(baseAddress, "v2/$repoName/manifests/$tag");
    final data = await http.get(dockerRegistryUrl, headers: {
      "Accept": "application/vnd.docker.distribution.manifest.v2+json",
      "Content-Type": "application/vnd.docker.distribution.manifest.v2+json"
    });

    final Map<String, dynamic> dataMap = jsonDecode(data.body);
    dataMap["digest"] = data.headers["docker-content-digest"];

    return dataMap;
  }

  String computeSha256(Map<String, dynamic> manifest) {
    manifest.remove("signatures");
    final strManifest = jsonEncode(manifest);
    return sha256.convert(utf8.encode(strManifest)).toString();
  }

  Future<void> deleteImage(String repoName, String sha256) async {
    final dockerRegistryUrl =
        Uri.http(baseAddress, "v2/$repoName/manifests/$sha256");
    var data = await http.delete(dockerRegistryUrl);
    debugPrint(data.body);
  }
}
