import 'package:card_actions/card_actions.dart';
import 'package:card_actions/card_action_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class TagCard extends StatelessWidget {
  final String tagName;
  final String digest;
  final Function onDelete;

  const TagCard(
      {super.key,
      required this.digest,
      required this.tagName,
      required this.onDelete});

  void showDeleteConfirmation(BuildContext context) {
    showDialog(
        context: context,
        builder: (ctx) {
          return AlertDialog(
            elevation: 3,
            icon: const Icon(Icons.delete_forever),
            title: Text("Delete Confirmation: $tagName"),
            content: Text(
              "Are you sure you want to delete tag $tagName."
              "\nThis action cannot be undone",
              style: const TextStyle(fontSize: 15),
            ),
            actions: [
              MaterialButton(
                onPressed: () {
                  Navigator.of(context).pop();
                  onDelete();
                },
                child: const Text(
                  "Yes",
                  style: TextStyle(color: Colors.redAccent),
                ),
              ),
              MaterialButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: const Text(
                  "No",
                  style: TextStyle(color: Colors.black),
                ),
              )
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return CardActions(
      buttonsCursor: SystemMouseCursors.click,
      backgroundColor: const Color(0xfAff7270),
      axisDirection: CardActionAxis.bottom,
      borderRadius: 5,
      width: 600,
      height: 200,
      actions: <CardActionButton>[
        CardActionButton(
          closeCardWhenPress: true,
          icon: const Icon(
            Icons.delete_forever,
            size: 25,
            color: Colors.white,
          ),
          label: 'Delete',
          onPress: () => showDeleteConfirmation(context),
        ),
        CardActionButton(
          closeCardWhenPress: true,
          icon: const Icon(
            Icons.copy,
            size: 25,
            color: Colors.white,
          ),
          label: 'Copy SHA256',
          onPress: () {
            Clipboard.setData(ClipboardData(text: digest));
            ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
              content: Text("Copied to clipboard!"),
            ));
          },
        ),
      ],
      child: Container(
        width: 600,
        height: 200,
        decoration: BoxDecoration(
          color: Colors.blueGrey,
          borderRadius: BorderRadius.circular(5),
          boxShadow: const [
            BoxShadow(
              color: Colors.black12,
              offset: Offset(5, 5),
              blurRadius: 5,
              // spreadRadius: 20,
            )
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: Column(
            children: [
              Row(
                children: [
                  const Text("Tag Name: ",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          color: Colors.white70)),
                  const Spacer(flex: 1),
                  Text(
                    tagName,
                    style: const TextStyle(color: Colors.white, fontSize: 16),
                  ),
                ],
              ),
              const Padding(
                padding: EdgeInsets.fromLTRB(0, 16, 0, 16),
                child: Divider(height: 1, color: Colors.white30),
              ),
              Container(
                alignment: AlignmentDirectional.centerStart,
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 8),
                child: const Text("SHA256: ",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                        color: Colors.white70)),
              ),
              Flexible(
                  child: Text(
                digest,
                style: const TextStyle(color: Colors.white, fontSize: 16),
              ))
            ],
          ),
        ),
      ),
    );
  }
}
