import 'package:flutter/cupertino.dart';

class TitleText extends StatelessWidget {
  final String title;

  const TitleText({super.key, this.title = ""});

  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      style: const TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
    );
  }
}
