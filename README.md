# dockerregistryexplorer

Simple project to explore private docker registry with tags info and digest

## Getting Started

This project is a Flutter application

### Prerequisites

- [Flutter](https://docs.flutter.dev/get-started/install)


## Running Project

- In project root run: `flutter pub get`
- To run this project simply in project root execute: `flutter run`


## License
This project is licensed under the Apache License 2.0
